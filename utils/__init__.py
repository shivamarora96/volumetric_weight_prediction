"""
         "prd_confidence": 0.52,
         "vol_q_10": 628.0,
         "vol_median": 987.0,
         "vol_q_90": 504000.0,
         "wt_q_90": 400.0,
         "wt_median": 160.0,
         "wt_q_10": 140.0,

         "rs": null,
         "snm": "1horn",
         "cl_vol": null,
         "rf_prd": null,
         "message": "1horn surface,,apparels,,1horn surface,0.52,cl-pl-snm-ctg,,,21,,,1horn,987.0,628.0,504000.0,13,160.0,140.0,400.0",
         "gm": null,
         "cl": "1horn surface",
         "ctg": "apparels",
         "pseg_scat": null,
         "pl": "1horn surface",
         "pseg_cat": null,

         "predicted_level": "cl-pl-snm-ctg",
         "remove": 21

"""

