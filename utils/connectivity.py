def check_connection(*args, **kwargs):
    def inner(func):
        def inner2(*args1, **kwargs1):
            http = kwargs['HTTP_CLIENT']
            url = kwargs['ES_URL']
            if url is None:
                raise Exception("ES URL IS NULL")
            if http is None:
                raise Exception("HTTP Client is NULL")

            try:
                r = http.request('GET', url)
            except Exception:
                raise Exception("ElasticSearch is down")
            res = func(*args1, **kwargs1)
            return res

        return inner2

    return inner