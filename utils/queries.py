BASE_QUERY = """


{
	"query":{
		"bool":{
			"must":[
				{
					"term":{
						"cl.keyword":"flipkart"
					}
				},
				{
					"term":{
						"remove":11
					}
				}
			]
		}
	}
}

"""

# Level0
# cl-pl-snm-rf_prd-pseg_scat-rs-gm-cl_vol


query_level_0 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":0
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},

				{
					"term":{
						"snm.keyword":"{}"
					}
				},


				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},


				{
					"term":{
						"pseg_scat.keyword":"{}"
					}
				},


				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"gm":{}
					}
				},


				{
					"term":{
						"cl_vol":{}
					}
				}
			]
		}
	}
}


"""

# Level1
# cl-pl-snm-rf_prd-pseg_cat-rs-gm-cl_vol

query_level_1 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":1
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},

				{
					"term":{
						"snm.keyword":"{}"
					}
				},


				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},


				{
					"term":{
						"pseg_cat.keyword":"{}"
					}
				},


				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"gm":{}
					}
				},


				{
					"term":{
						"cl_vol":{}
					}
				}
			]
		}
	}
}


"""

# Level -2
# cl-pl-snm-rf_prd-ctg-rs-gm-cl_vol


query_level_2 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":2
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},

				{
					"term":{
						"snm.keyword":"{}"
					}
				},


				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},


				{
					"term":{
						"ctg.keyword":"{}"
					}
				},


				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"gm":{}
					}
				},


				{
					"term":{
						"cl_vol":{}
					}
				}
			]
		}
	}
}


"""

# Level-3
# cl-pl-snm-rf_prd-rs-gm-cl_vol


query_level_3 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":3
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},

				{
					"term":{
						"snm.keyword":"{}"
					}
				},


				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},

				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"gm":{}
					}
				},


				{
					"term":{
						"cl_vol":{}
					}
				}
			]
		}
	}
}


"""

# Level-4
# cl-pl-rf_prd-rs-gm-cl_vol

query_level_4 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":4
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},


				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},

				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"gm":{}
					}
				},


				{
					"term":{
						"cl_vol":{}
					}
				}
			]
		}
	}
}


"""


#Level-5
# cl-pl-snm-rf_prd-rs-gm

query_level_5 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":5
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},

                {
					"term":{
						"snm.keyword":"{}"
					}
				},

				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},

				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"gm":{}
					}
				}
			]
		}
	}
}


"""

# Level-6
# cl-pl-snm-rf_prd-rs-cl_vol

query_level_6 = """


{
	"query":{
		"bool":{
			"must":[

				{
					"term":{
						"remove":6
					}
				},

				{
					"term":{
						"cl.keyword":"{}"
					}
				},
				{
					"term":{
						"pl.keyword":"{}"
					}
				},
				
                {
					"term":{
						"snm.keyword":"{}"
					}
				},

				{
					"term":{
						"rf_prd.keyword":"{}"
					}
				},

				{
					"term":{
						"rs":{}
					}
				},


				{
					"term":{
						"cl_vol":{}
					}
				}
			]
		}
	}
}


"""
